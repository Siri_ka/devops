FROM php:apache
RUN apt-get update \
        && apt-get install -y apache2 \
        && apt-get install -y libgs9-common \
        && apt-get clean \
        && rm -rf /var/lib/apt/lists/*

ADD src /var/www/html
EXPOSE 80
CMD ["/usr/sbin/apache2ctl","-D","FOREGROUND"]
